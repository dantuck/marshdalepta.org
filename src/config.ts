export const SITE = {
  site: "https://marshdalepta.org",
  title: "Marshdale PTA",
  description: "The Marshdale PTA website for Marshdale Elementary School in Evergreen, Colorado.",
};

export const OPEN_GRAPH = {
  locale: "en_US",
  image: {
    src: "/img/opengraph.webp?v=1",
    alt:
      "Marshdale Elementary new building rendition",
  },
};
