---
layout: '../../layouts/BlogPost.astro'
title: King Soopers Community Rewards
# description: Introducing Marshdale PTAs new website.
# date: 2022-07-29
image: /img/community_rewards_hro_kingsoopers_community_rewards.png
imageAlt: Marshdale 2022 new building rendition
---

The Kroger Family of Companies is committed to community engagement, positive social impact and charitable giving at the national and local levels. Every community is unique, but our common goal is to partner with the neighborhoods we serve and help the people there live healthier lives.

One of the ways in which we do this is through our King Soopers Community Rewards program. This program makes fundraising easy by donating to local organizations based on the shopping you do every day. Once you link your Card to an organization, all you have to do is shop at King Soopers and swipe your Shopper’s Card. Here’s how it works:

**1. Create a digital account.**

A digital account is needed to participate in King Soopers Community Rewards. If you already have a digital account, simply link your Shopper’s Card to your account so that all transactions apply toward the organization you choose.

**2. Link your Card to an organization.**

Selecting the organization that you wish to support is as simple as updating the King Soopers Community Rewards selection on your digital account.

    1. Sign in to your digital account.

    2. Search for your organization [here](https://www.kingsoopers.com/account/communityrewards).

    3. Enter the name or NPO number of the organization you wish to support.

    4. Select the appropriate organization from the list and click “Save”.

Your selected organization will also display in the King Soopers Community Rewards section of your account. If you need to review or revisit your organization, you can always do so under your Account details.

**3. Your organization earns.**

Any transactions moving forward using the Shopper’s Card number associated with your digital account will be applied to the program, at no added cost to you. King Soopers donates annually to participating organizations based on your percentage of spending as it relates to the total spending associated with all participating King Soopers Community Rewards organizations.

#### Whether you’re a customer or an organization, get started today!

Note: If you are a customer, make sure you have a preferred store selected to view participating organizations. If you are applying on behalf of an organization, please select a store in the same area as your organization.