---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Oct 09, 2022
description: Marshdale News - Oct 09, 2022
date: 2022-10-10
image: /img/news-default.webp
imageAlt: Marshdale News - Oct 09, 2022
---

**THANK YOU MARSHDALE!** This year's Mustang Trot was a HUGE success! Thank you, Lara Nusbaum and the Mustang Trot committee for all of your hard work with this fundraiser. Marshdale is so lucky to have you!

**Join the PTA** - Membership is record breaking this year!
https://www.marshdalepta.org/membership/

#### Pumpkin Decorating Contest

Pumpkins are due October 25th, winners receive a gift bag from Critters.

[Grab the flyer and read more](/assets/2022_Pumpkin_Decorating_Flyer.pdf)

#### Marshdale Pumpkin Festival

Join the Marshdale community for the annual Pumpkin Festival on October 30th from 11am-2pm. [Details](https://marshdalepta.memberhub.com/store/items/784277) and [volunteer opportunities](https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-pumpkin).

**Show your voice!** Students have the chance to express themselves through art in the Reflections program. Projects due November 9. [Find complete details here](/enrichment/reflections).

Help us feed our hardworking teachers and staff during fall break as they move 40 years of life at Marshdale into our new building.
https://www.signupgenius.com/go/5080a45adaa28a2f49-legacy

#### PTA General Meeting Child Care Available!

Marshdale PTA will be providing child care so you can attend the General Meeting on Tuesday October 11th @ 5:30 pm. Please use this Link to let us know that your kiddo(s) will be attending. 

#### King Soopers Community Rewards

Log into your account and click on Community Rewards on the left and choose Marshdale Elementary PTA. Costs nothing! [Read more about how to get setup here](/fundraising/kingsoopers-community-rewards/)
