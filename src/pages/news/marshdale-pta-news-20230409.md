---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Apr 09, 2023
description: Marshdale PTA News - Apr 09, 2023
date: 2023-04-10
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Apr 09, 2023
---

#### Spring Auction

Would you like to volunteer at the Spring Auction or help before the auction? We need you!

https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-spring

#### Yearbook

Order your yearbook today and you can work on the two custom pages that will only appear in your book. The Yearbook Team is excited to share their hard work with you. Orders and custom pages need to be completed by April 18th. You can still order after April 18th, but you will have to pay for shipping directly to your home.

Follow this link to order: 
https://www.treering.com/purchase?PassCode=1016528018343197

#### 5th Grade Parents 

Please remember to turn in your form for the free graduation yard sign. They are due Wednesday, April 12th.

West Jeff Middle School PTA has several open positions: President, Treasurer, VP of Communications, VP of Enrichment & VP of Fundraising. Reach out to WJMSPTA@gmail.com if interested.

#### Green Team

Battery Recycling Contest! Earth day is Saturday April 22nd and the week before (Mon 4/17 - Fri 4/21) we will be having a battery recycling contest! Batteries are not meant to be thrown in the trash and should be recycled. Each classroom will collect old household batteries and the classroom who collects the most will get a popcorn party! 

Also, we need some volunteers for our next composting days during school lunch on 4/12 and 5/10. Follow the link to sign up or email laclark818@gmail.com with any questions!
[Marshdale Composting: Marshdale Composting (signupgenius.com)](https://www.signupgenius.com/go/70a0f44adae2ea4f49-marshdale#/)

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
