---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Aug 20, 2023
description: Marshdale PTA News - Aug 20, 2023
date: 2023-08-21
# image: /img/news-default.webp
# imageAlt: Marshdale PTA News - Aug 13, 2023
---

#### Membership

The Marshdale PTA Membership Drive is now OPEN! Join the PTA - all the rights with none of the requirements. 
Family Membership $35 (includes 2 members)
Individual Caregivers $20
https://marshdalepta.memberhub.com/store?limit=21&live=true

#### Marshdale Monday

Please join us at Marshdale Burger on Monday, August 21st from 5:30-7:00 for Marshdale Monday. Come meet new and old friends and learn about PTA. 

#### Mustang Trot 2023

Mustang Trot September 29th!  Register before September 6th!

The Mustang Trot is a very important, fun, and healthy way to raise money for the students of Marshdale Elementary!

We want to promote a healthy lifestyle to our students while fundraising for our incredible school.  The money we raise from the Mustang Trot will help us purchase safety enhancements for our school!

<u>**There are 3 simple steps to reach our goal quickly!**</u>

1. First, register each of your students by either scanning the QR code above or going to https://app.memberhub.gives/trotregistration.   There is no obligation to fundraise but <u>**register by September 6th**</u>! will ensure your child will get the correct size t-shirt and have their name in a drawing for great prizes such as free ice cream from Liks, toys from Critter’s Toy Store!  Sooner you register, the more chances they have to win!  Drawings are each week until September 6th!  AND the first 3 classes that have 100% registration get a popcorn party!

2. If you are able (no obligation), please donate.  Kids can win different prizes and there is even a family incentive this year!  See incentive sheet.

3. Share with friends and family; they want to help!  You would be surprised how friends and family want to help.  

    - Copy the link from the website and share.
    - Post on Facebook or Twitter
    - Help your child reach their goal quickly!  Look at the incentive sheet!

<u>**Our School Goal:**</u> $45,000. Raising money for safety enhancements for the school.

<u>**Event Details:**</u>  Mustang Trot September 29th, 2023.

<u>**Colorful Run:**</u>  We will have each grade representing a color from the rainbow.  We ask your student to be as creative as possible the day of the Trot and to wear as much of their grade color as possible.  Paint their face, wear sneakers that color, or color their hair.  Let’s make this fun and creative!

5th Grade: <span style="color:red">Red</span><br />
4th Grade: <span style="color:orange">Orange</span><br />
3rd Grade: <span style="color:gold">Yellow</span><br />
2nd Grade: <span style="color:green">Green</span><br />
1st Grade: <span style="color:blue">Blue</span><br />
Kindergarten:  <span style="color:indigo">Indigo</span> (What is this?  Rich color between blue and violet. Do your best!)<br />
Pre-school:  <span style="color:violet">Violet</span><br />

Kids will run through colorful blow-up arches that are rainbow colors.  We will have parents holding pool noodles in different colors that students will run through before they get their colored lap bracelet!

<u>**Healthy Eating:**</u>  Mr. Chambers will be talking about which foods give you more energy and why it is important to eat healthy.

<u>**Parent Signs of Encouragement:**</u>  Start thinking about whether you want to create a sign to cheer your student on.  This year we will not be putting them on the fence.  We ask the parents attending to hold them, so it is easier for the students to see them! 

<u>**Want to Volunteer?**</u>  Contact fundraising@marshdalepta.org or look for sign-ups through Mr. Martin’s Sunday emails, lead parent email or our Marshdale Elementary Community Facebook Page.

**Thank you for helping us make this year’s Mustang Trot the most successful year yet!**

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
