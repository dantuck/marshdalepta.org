---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - May 28, 2023
description: Marshdale PTA News - May 28, 2023
date: 2023-05-29
image: /img/news-default.webp
imageAlt: Marshdale PTA News - May 28, 2023
---

#### A Note From the Marshdale PTA Board

Dear Marshdale Families, 

Thank you for allowing us to serve Marshdale this past school year. It was truly one for the books - from the first day of school, the Mustang Trot, Pumpkin Festival, to moving into the new building, the Spring Auction and everything in between! Thank you to everyone who joined the PTA during our membership drive, donated to fundraisers, volunteered their time and every other way you all supported us in supporting our incredible school.

Thank you! Happy summer break!

#### School Supplies

Don't have school supply envy when we return back to school! Order your school supply kit today. Deadline is May 31st!

[Order your 1st Day School Supplies](https://1stdayschoolsupplies.com/kits.php?sid=19008)

#### Marshdale Green Team

Thank you to everyone who participated in the Battery Recycling Contest! We were able to recycle 410 lbs of batteries at R2 eCycle in Denver! If you have any recycling to do over the summer you should check them out, or start saving up your household batteries for the contest next year!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
