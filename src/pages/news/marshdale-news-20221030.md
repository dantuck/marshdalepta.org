---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Oct 30, 2022
description: Marshdale News - Oct 30, 2022
date: 2022-10-31
image: /img/news-default.webp
imageAlt: Marshdale News - Oct 30, 2022
---

Thank you, Ashley Remington, the Pumpkin Festival committee and volunteers for a wonderful Pumpkin Festival today. We could not put on fun family events like this with the support of our community - **THANK YOU!**

Thank you for all of the Pumpkin Decorating Contest submissions, we had over 50 this year! Congratulations to our Pumpkin Decorating Contest winners!

We need volunteers to feed our hard working parents on Thursday, November 3 for Parent Teacher Conferences. https://m.signupgenius.com/#!/showSignUp/5080A45ADAA28A2F49-dinner

#### October is National Principals Month

Our school is so fortunate to have Mr. Martin leading our students and staff. The leadership, support, and energy that he provides to our schools on a daily basis is at the core of what makes our school great. Thank you, Mr. Martin!

#### Show your voice!

Students have the chance to express themselves through art in the Reflections program. Projects due November 9. Find complete details at https://www.marshdalepta.org/enrichment/reflections.

#### Marshdale's yearbook is now available to purchase! 

Order by October 31 to receive 10% off. Design two custom pages for free that will show up only in your book. Go to <a href="https://www.treering.com/validate" target="_blank">https://www.treering.com/validate</a> and enter our passcode: **1016528018343197**

**Join the Marshdale PTA** - https://www.marshdalepta.org/membership/
