---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Aug 11, 2024
description: Marshdale PTA News - Aug 11, 2024
date: 2024-08-12
---

#### New Parent Breakfast

Get to know the new families at Marshdale.  All new family members are welcome to a breakfast at the main playground after drop-off on Thursday, August 15th.

Interested in being involved with Mustang Trot this year? Mustang Trot will be held on Friday, October 4th. The committee is hosting a kick-off call on Tuesday, August 13th, from 6:00 PM - 7:00 PM on Zoom. Passcode is “trot.” All are welcome to join!

#### PTA Business Sponsors

Would your business like to be a PTA business sponsor? Please reach out to fundraising@marshdalepta.org. Your logo will be on the Mustang Trot T-shirt and you will help fund the many things that the PTA does at Marshdale such as giving each teacher $350 to spend in their classroom.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
