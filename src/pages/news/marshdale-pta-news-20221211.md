---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Dec 11, 2022
description: Marshdale PTA News - Dec 11, 2022
date: 2022-12-12
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Dec 11, 2022
---

#### Fence Signs

Thank you to our Volunteers, who hang business sponsor fence signs for Marshdale PTA: Daniel Tucker, Dan Ford & Jacob Ware.

#### Science Fair

Mark your calendars for the **2023 Marshdale Science Fair**! It's never too early to begin working on your project. Register your project plan by February 8, and final projects will be due February 22. **Pick up your free display board from the office**.

Find complete details at https://www.marshdalepta.org/enrichment/science-fair/

Even if your child is entering a project, please consider volunteering to help make our Science Fair a success. If you are willing to give 30 minutes to 2 hours of your time to help judge the projects, please sign-up for one of our judging sessions. https://www.signupgenius.com/go/4090E45A4A72DA7FC1-marshdale

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
