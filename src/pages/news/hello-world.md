---
layout: '../../layouts/BlogPost.astro'
title: Hello, world
description: Introducing Marshdale PTAs new website.
date: 2022-08-10
image: /img/marshdale-new-building.webp
imageAlt: Marshdale 2022 new building rendition
---

We're excited to announce Marshdale PTA has a new website.
