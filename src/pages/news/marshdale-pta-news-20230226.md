---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Feb 26, 2023
description: Marshdale PTA News - Feb 26, 2023
date: 2023-02-27
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Feb 26, 2023
---

#### Sprit Week & Dr. Seuss Week is February 27th - March 3rd!

All students are welcome to participate:

- Monday - Hat Day
- Tuesday - Crazy sock Day
- Wednesday - Wacky Wednesday 
- Thursday - Wocket in my Pocket
- Friday - Crazy Hair Day

#### Parent Teacher Conference Meals for Teachers & Staff

Parent/Teacher Conferences are coming up on March 2nd and March 9th.  We will be providing dinner for our hard working teachers. 

If you would like to contribute to the dinners please sign up at https://www.signupgenius.com/index.cfm?go=w.manageSignUp#/12374085/slots/

#### A Day Without Hate

A Day Without Hate is a Jeffco student-led, grassroots organization that promotes nonviolence, respect, and unity within our schools. Marshdale's student leadership will lead our school in activities surrounding these values on April 27th. If you would like to order a Day Without Hate t-shirt (proceeds go to scholarships for Jeffco students), forms will be found in your child's Friday Folder this week. Orders are due by March 8th.

Contact wendi.vanlake@marshdalepta.org if you have any questions.

#### Yearbook

Order your yearbook today and you can work on the two custom pages that will only appear in your book. The Yearbook Team is excited to share their hard work with you. Orders and custom pages need to be completed by April 18th. You can still order after April 18, but you will have to pay for shipping directly to your home.

Follow this link to order: https://www.treering.com/purchase?PassCode=1016528018343197

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
