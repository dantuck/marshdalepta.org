---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Oct 01, 2023
description: Marshdale PTA News - Oct 01, 2023
date: 2023-10-02
---

#### Membership

Thank you to all of our PTA Members! We closed out our membership drive on Saturday with record breaking membership numbers!

#### Mustang Trot

Thank you to everyone who donated, volunteered and helped to support our annual Mustang Trot! We could not do it without you. Final numbers will be posted soon. 

The PTA Board would also like to thank Lara Nusbaum for all of her incredible hard work putting the Mustang Trot together. The Trot is a big event and brings in thousands of dollars to our school. Thank you, Lara!

#### PTA General Meeting

Tuesday, October 10th at 5:30 pm - Please join us for a presentation from Scott Cypers, PhD, Associate Professor / Licensed Psychologist and Director of Stress & Anxiety Programs at the Helen and Arthur E. Johnson Depression Center, University of Colorado School of Medicine

#### Pumpkin Festival

Join us on **October 15th** from **11am-2pm** at Marshdale Elementary School for our annual Pumpkin Festival.   Festival admission and games are FREE.  The fun includes a bounce slide, carnival games with prizes, face painting, a treat walk and food for purchase from local food trucks.  **Please RSVP** [HERE](https://docs.google.com/forms/d/17mKsHQGUV3Cf06FVrJrnqSWbFnDZvBgddTWF41MBVTI/edit) to receive a prize bag and a free bag of popcorn.  All community members and friends are welcome.

If you would like to help run games at the Pumpkin Festival.  Please sign up [HERE](https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-44456099-pumpkin).

Google Form to RSVP:<br />
[Marshdale Pumpkin Festival RSVP - Google Forms](https://docs.google.com/forms/d/17mKsHQGUV3Cf06FVrJrnqSWbFnDZvBgddTWF41MBVTI/edit)

Signup Genius for Pumpkin Festival:<br />
https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-44456099-pumpkin

PTA Shout Outs - Thank you to...

Laura Squires for providing lunch to teachers & staff on 9/22.<br />
Natalie Ollhausen for stepping up and serving as Room Parent Coordinator.<br />
Rachel Gaffney for taking over Marshdale Singers so that we can continue to offer Singers to our students.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
