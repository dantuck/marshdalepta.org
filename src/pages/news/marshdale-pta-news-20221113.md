---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Nov 13, 2022
description: Marshdale PTA News - Nov 13, 2022
date: 2022-11-14
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Nov 13, 2022
---

#### New Enrichment Opportunities!

Check out the enrichment page for two new activity - [Enrichment activities](/enrichment/activities/).

- Eric Stone Guitar
- Little Medical School 

#### Green Team

Are you wondering how the cafeteria looks in the new school? Sign up to help the kids compost during a school lunch and check it out! Composting happens every second Wednesday during school lunch from 11:00am - 1:15pm. Sign up link is below!

Email Laura Rios at laura.clark.rios@gmail.com if you have any questions.

[Marshdale Composting: Marshdale Composting (signupgenius.com)](https://www.signupgenius.com/go/70a0f44adae2ea4f49-marshdale)
