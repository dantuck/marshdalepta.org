---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Sept 3, 2023
description: Marshdale PTA News - Sept 3, 2023
date: 2023-09-04
---

#### PTA Membership

The Marshdale PTA Membership Drive is now OPEN! [Join the PTA](/membership/) - all the rights with none of the requirements.

Family Membership $35 (includes 2 members)<br/>
Individual Caregivers $20<br/>

https://marshdalepta.memberhub.com/store?limit=21&live=true

#### The Big Chili

Get into the Big Chili Cook-Off for free!  Marshdale Elementary PTA is going to be selling water at the Big Chili Cook-Off on September 9th.  Help volunteer and get in for free! https://www.signupgenius.com/go/10c0e44a8a62aa1f4cf8-water#/

#### Patriot's Day

Marshdale will hold its annual Patriot's Day ceremony and Heroes Breakfast on Monday, September 11th. Please consider donating a dish or food item for the breakfast. We will be feeding our local heroes - First Responders, Active Military and Veterans.

https://www.signupgenius.com/go/4090E45A4A72DA7FC1-patriots

#### Mustang Trot

You must register by this Wednesday, September 6th for your child to get the correct size t-shirt and be entered to win several registration prizes!

We are raising money for safety enhancements at the school. http://app.memberhub.gives/mustang-trot

We made our next goal for Mustang Trot!  Crazy Hair Day on Wednesday, September 6th!

Volunteer at the Mustang Trot: https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-mustang#/

#### Spiritwear

Marshdale Spiritwear 30% off until September 7th and free shipping with minimum order! https://1stplace.sale/16465

#### Yearbooks for Sale!

Buy your 23/24 Marshdale Yearbook today! Follow this link to set up your account and receive 10% off any purchase before September 30.

https://www.treering.com/purchase?PassCode=1016528018343197

Families of 5th graders, please set up your account as soon as possible, but do not purchase a yearbook. The PTA will buy your yearbook, but you must have an account before September 8.

#### Green Team

Come join the Green Team for the first meeting of the year on Thursday 9/7 from 4-5 pm in the Art Room! Both students and parents are welcome to join. We will do an activity and discuss green activities for the school, so bring any ideas that you might have!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
