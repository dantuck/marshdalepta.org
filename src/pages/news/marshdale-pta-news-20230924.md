---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Sept 24, 2023
description: Marshdale PTA News - Sept 24, 2023
date: 2023-09-25
---

#### PTA Membership - It's the Final Countdown!

We have entered the final week of our Membership Drive. [Join today](/membership/)! - You get all of the rights without any requirements.

[Family Membership](https://marshdalepta.memberhub.com/store/items/739352) $35 (includes 2 members)<br/>
[Individual / Caregivers](https://marshdalepta.memberhub.com/store/items/104447) $20<br/>

https://marshdalepta.memberhub.com/store?limit=21&live=true

#### RISE Store Returns

Our Rise Store is returning and there are opportunities for family to celebrate with their students.

https://www.signupgenius.com/go/8050445AEA92DAAFE3-44386390-rise#/

#### Mustang Trot this Friday - come cheer your Mustang on!

**When**: Marshdale Field 9:30-10:30am!

Last push to fundraise. We are raising money for safety enhancements!

Look for your student to take home their Mustang Trot T-shirt (and any extras you ordered) this Thursday for them to wear Friday.

If you would like to volunteer: https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-mustang#/

We will have each grade representing a different color from the rainbow.  We ask students to be as creative as possible the day of the Trot and to wear as much of their grade color as possible.  Paint their face, wear sneakers that color, or color their hair.  Let’s make this fun and creative!

5th Grade: <span style="color:red">Red</span><br />
4th Grade: <span style="color:orange">Orange</span><br />
3rd Grade: <span style="color:gold">Yellow</span><br />
2nd Grade: <span style="color:green">Green</span><br />
1st Grade: <span style="color:blue">Blue</span><br />
Kindergarten: <span style="color:indigo">Indigo</span> (What is this?  Rich color between blue and violet. Do your best!)<br />
Pre-school: <span style="color:violet">Violet</span><br />

#### Marshdale Pumpkin Festival

Join us on October 15th from 11am-2pm at Marshdale Elementary School for our annual Pumpkin Festival.

Festival admission and games are FREE. The fun includes a bounce slide, carnival games with prizes, face painting, a treat walk and food for purchase from local food trucks.  

Please RSVP &rarr; [HERE](https://docs.google.com/forms/d/17mKsHQGUV3Cf06FVrJrnqSWbFnDZvBgddTWF41MBVTI/viewform) to receive a prize bag and a free bag of popcorn. All community members and friends are welcome.

If you would like to help run games at the Pumpkin Festival. Please sign up &rarr; [HERE](https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-44456099-pumpkin).

#### Yearbooks for Sale!

Buy your 23/24 Marshdale Yearbook today! Follow this link to set up your account and receive 10% off any purchase before September 30.

https://www.treering.com/purchase?PassCode=1016528018343197

Have you taken photos of our Marshdale Mustangs?

The Yearbook Club would love to consider them for this year’s book. Please share photos with us in whichever way is easiest for you:

1. Submit via a Google Form: https://forms.gle/ebjiwYfA96nhcumF9
2. Upload directly to the Treering Yearbook site. You will need an account to do this: https://tr5.treering.com/school/242283/2023/school-content/school-photos
3. Share a Google Photo Album or email directly to yearbook@marshdalepta.org

Thank you for helping us capture our cherished memories this school year! Contact yearbook@marshdalepta.org with any questions.

#### Winter and Spirit Wear Clothing Fundraiser

The Marshdale Green Team will be collecting gently used winter clothing (hats, gloves, jackets, snow pants and boots) and Marshdale spirit wear from Monday 10/9 through Friday 10/13 in the school office. The Green Team will be selling the winter clothing and spiritwear at the Pumpkin Festival on October 15th from 11am-2pm. The proceeds will go towards a compost area at Marshdale! A great way to clear out your closet and help out the school!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
