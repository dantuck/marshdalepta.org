---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Sept 10, 2023
description: Marshdale PTA News - Sept 10, 2023
date: 2023-09-11
---

#### PTA Membership

The Marshdale PTA Membership Drive is now OPEN! [Join the PTA](/membership/) - all the rights with none of the requirements.

Family Membership $35 (includes 2 members)<br/>
Individual Caregivers $20<br/>

https://marshdalepta.memberhub.com/store?limit=21&live=true

#### Mustang Trot

Congratulations Mustangs we have hit our next goal of $20,000!  So Mr. Martin will be helping to serve lunch in the cafeteria wearing his chef's hat one day this week!  Keep fundraising!  We are halfway to our goal to raise money for safety enhancements.  

If you would like to volunteer at the Mustang Trot:  

https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-mustang#/

#### Yearbooks for Sale!

Buy your 23/24 Marshdale Yearbook today! Follow this link to set up your account and receive 10% off any purchase before September 30.

https://www.treering.com/purchase?PassCode=1016528018343197

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
