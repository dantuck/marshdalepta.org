---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Aug 13, 2023
description: Marshdale PTA News - Aug 13, 2023
date: 2023-08-14
# image: /img/news-default.webp
# imageAlt: Marshdale PTA News - Aug 13, 2023
---

#### JOIN THE PTA!

The Marshdale PTA membership drive is now open! Membership gives you rights without requirements. Have your voice heard - voting rights. No volunteering required (always welcome & appreciated though :)

[Family Membership - $35](https://marshdalepta.memberhub.com/store/items/739352)

[Individual Parent/Guardian - $20](https://marshdalepta.memberhub.com/store/items/104447)

#### New Parent/New Family Breakfast

Welcome Pre-School, Kindergarten and new families to Marshdale Elementary School! We are so excited to have you. Please join us for an informational presentation and breakfast on Tuesday, August 15th at 8 am in the Courtyard outside the STEM room. Psst! We'll have plenty of tissues on hand too.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
