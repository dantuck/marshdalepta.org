---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Dec 18, 2022
description: Marshdale PTA News - Dec 18, 2022
date: 2022-12-19
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Dec 18, 2022
---

A message from Marshdale PTA President, Abby Redwine:

> Thank you to everyone who donated to the Mountain Backpack Program. We contributed over 101 lbs of food and supplies.  
>
> Happy Holidays and a Happy New Year from the Marshdale PTA. We look forward to seeing you all in 2023!

#### Reflections

Congratulations to Kindergartener Emmot Tucker for winning the Jeffco Reflections contest! He was recognized at a district level ceremony on Friday and his visual arts project will advance to the state level. Way to go, Emmot!

#### Science Fair

Mark your calendars for the **2023 Marshdale Science Fair**! It's never too early to begin working on your project. Register your project plan by February 8, and final projects will be due February 22. **Pick up your free display board from the office**.

Find complete details at https://www.marshdalepta.org/enrichment/science-fair/

Even if your child is entering a project, please consider volunteering to help make our Science Fair a success. If you are willing to give 30 minutes to 2 hours of your time to help judge the projects, please sign-up for one of our judging sessions.

https://www.signupgenius.com/go/4090E45A4A72DA7FC1-marshdale

#### Yearbook

Buy your yearbook today! Each yearbook will have two free customizable pages that will only be printed in your book. If you choose to donate a book to a Marshdale student in need by December 31, Treering will match your donation. Just click on "donate a yearbook" at checkout. 

https://www.treering.com/purchase?PassCode=1016528018343197

#### Enrichment Activities

Several after-school enrichment opportunities are available to Marshdale students. New programs and new sessions of existing programs are beginning in January. Check out the offerings at https://www.marshdalepta.org/enrichment/activities/

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
