---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Oct 08, 2023
description: Marshdale PTA News - Oct 08, 2023
date: 2023-10-09
---

#### Stay in know with Marshdale Elementary School & PTA

https://www.facebook.com/groups/marshdaleelementarycommunity<br />
https://www.facebook.com/marshdaleelementarypta<br />
https://www.marshdalepta.org/<br />

#### Membership Drive

Mrs. Davidson's class won the PTA membership drive so they will receive a popcorn party! Way to go, First Grade Parents!

#### Mustang Trot

Thank you Mustang Trot volunteers! We could not put together or pull off a fundraiser like the Mustang Trot without the help of so many volunteers. Thank you for the help and support!

#### Pumpkin Festival

Join us on **October 15th** from **11am-2pm** at Marshdale Elementary School for our annual Pumpkin Festival.   Festival admission and games are FREE.  The fun includes a bounce slide, carnival games with prizes, face painting, a treat walk and food for purchase from local food trucks.  **Please RSVP** [HERE](https://docs.google.com/forms/d/17mKsHQGUV3Cf06FVrJrnqSWbFnDZvBgddTWF41MBVTI/edit) to receive a prize bag and a free bag of popcorn.  All community members and friends are welcome.

If you would like to help run games at the Pumpkin Festival.  Please sign up [HERE](https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-44456099-pumpkin).

Google Form to RSVP:<br />
[Marshdale Pumpkin Festival RSVP - Google Forms](https://docs.google.com/forms/d/17mKsHQGUV3Cf06FVrJrnqSWbFnDZvBgddTWF41MBVTI/edit)

Signup Genius for Pumpkin Festival:<br />
https://www.signupgenius.com/go/4090A49AEAE2BA3FF2-44456099-pumpkin

PTA Shout Outs:

Thank you, Wendy Thurman for all your help with PTA weekly announcements & news!<br />
Thank you, Dan Ford & Daniel Tucker for helping with fence signs (again!)!<br />
Thank you, Terry Brunswick, for taking such great care of our school!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
