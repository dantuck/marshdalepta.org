---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Apr 16, 2023
description: Marshdale PTA News - Apr 16, 2023
date: 2023-04-17
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Apr 16, 2023
---

#### Spring Auction

**THANK YOU!** Thank you to everyone who attended our spring auction on Friday evening. Thank you to the PTA board and volunteers who made this event possible. We will share final numbers as soon as possible. Thanks!

#### Yearbook

This is it! It's time to create your two custom pages if you would like them printed in your yearbook for your friends to sign on the last day of school. Create your own pages with pictures of your pets, families, sports, and activities that will only be printed in your book. Order and create these pages by this Tuesday, April 18th. If you order after this date, you will have to pay shipping and will not have your book in time for autographs from your classmates.

Follow this link to order: 
https://www.treering.com/purchase?PassCode=1016528018343197

#### Fence Signs

Marshdale PTA fence sign sponsorship program brings in revenue for the PTA, which in turn helps fund programs for Marshdale. This program would not exist without our volunteers that physically hang business sponsor fence signs. 

Thank you to our volunteers:

Daniel Tucker<br />
Dan Ford<br />
Jacob Ware<br />
Alan Skerl<br />

#### School Store

The RISE School Store needs parent volunteers to help on Wednesday, April 26th, from 1-3 and Thursday, April 27th, from 11-3. The kids have been earning RISE dollars and are excited to use them. Each class will be assigned a specific time. A sign up genius link will go out next week so please consider helping during your student's designated time or whenever you have time. We appreciate it!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
