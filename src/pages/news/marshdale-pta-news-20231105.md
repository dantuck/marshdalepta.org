---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Nov 05, 2023
description: Marshdale PTA News - Nov 05, 2023
date: 2023-11-06
---

#### Reflections

PTA Reflections - Explore the Arts and Express Yourself
This year the theme is "I am Hopeful Because…".

Info went home in Friday folders. More info at https://www.pta.org/home/programs/reflections.

Reflections are due Wednesday, November 8.

#### Honor a Veteran

Marshdale will observe Veterans Day, November 10th by displaying a "Wall of Heroes." Each student is invited to write a biography (or biographies) to honor family members, or friends, who have served, or are currently serving in the United States Armed Forces. Please return this [FORM](https://docs.google.com/presentation/d/1SM0qsPTTVykDuVtqmxdy2P37msLVBmoSfowPOUkpN7E/edit#slide=id.p) by November 7th so they can be displayed for Veterans Day.

#### After-School Enrichment

Check out the after school enrichment programs offered by the PTA.

https://www.marshdalepta.org/enrichment/activities/

#### PTA Shout Outs

Thank you, Jake Ware, for being a PTA fence sign volunteer and hanging so many of our sponsor's signs!<br />
Thank you, David Oyler, and welcome to Marshdale! Thank you for engaging our 5th graders and preparing them for middle school.<br />
Thank you, Dan Ford, for being one of Marshdale's fantastic paraeducators! Dan is always willing to jump in and help where needed.<br />

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
