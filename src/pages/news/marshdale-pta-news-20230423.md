---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Apr 23, 2023
description: Marshdale PTA News - Apr 23, 2023
date: 2023-04-24
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Apr 23, 2023
---

#### Spring Auction Update!

Thank you everyone for our most successful Spring Auction to date!  We had a record number of people and raised **$42,245 total**! That is the most we have ever raised at the Spring Auction!  Keep an eye out as they start pouring the cement and putting together our new playground and our exciting basketball court with adjustable hoops!

#### School Store

The RISE School Store needs parent volunteers on Wednesday, April 26. The kids have been working hard to earn RISE dollars and are excited to spend them.  If you can help out please click on the link https://www.signupgenius.com/go/5080A45ADAA28A2F49-marshdale2 and sign up.

#### Day A Without Hate

This Thursday, April 27th, Marshdale will recognize A Day Without Hate. DWOH is a grassroots movement by the students of Jeffco focused on raising awareness about school violence. If your child ordered a T-shirt, it will go home this week and should be worn on Thursday 4/27.

#### Teacher Appreciation

Teacher and Staff Appreciation is going to be celebrated at Marshdale on May 1-5.  PTA has several activities planned to say "Thank You" to our wonderful teachers and staff.  We are asking you to please celebrate your teachers and our staff on Thursday, May 4. Also, Administrative Professionals Day is on Wednesday, April 26 and Principals' Day is May 1, if your family would like to recognize Mrs. Woodworth, Mrs. Thurman, Mrs. Haydon and Mr. Martin. 

#### Earth Day

Evergreen Sustainability Alliance is hosting a day of community clean-ups at our local mountain schools.

Saturday, April 22nd 10 am - Noon<br />
Marshdale Elementary School<br />
See [flyer](/assets/2023-earth-day.pdf) for more details.

#### Battery Recycling

Thank you to everyone who participated in the battery recycling contest! The winning classroom will be announced on Monday 4/24. 

The Green Team also provided seed packets with Colorado Wildflowers that were handed out on Friday in celebration of Earth Day! Each packet has a variety of flowers that are specific to Colorado and are non-gmo seeds. The planting instructions should be on the packet, but they require full sun to partial shade and should be planted 1/4" deep in soil. 

Hope everyone had a great Earth Day on Saturday!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
