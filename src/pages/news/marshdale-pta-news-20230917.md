---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Sept 17, 2023
description: Marshdale PTA News - Sept 17, 2023
date: 2023-09-18
---

#### PTA Membership

The Marshdale PTA Membership Drive is now OPEN! [Join the PTA](/membership/) - all the rights with none of the requirements.

Family Membership $35 (includes 2 members)<br/>
Individual Caregivers $20<br/>

https://marshdalepta.memberhub.com/store?limit=21&live=true

#### Mustang Trot

Keep fundraising for school safety enhancements!  We reached our next goal of $25,000 so look out for the front office to dress like superheroes!  We are at 55% of our goal so let's make the home stretch the most successful week!  Reach out on Facebook, email friends and family.  Our next goal is everyone gets to wear PJs to school!  And of course there are individual prizes for students and families!  

Congratulations to Mrs. Boman's class for raising the most money last week!  They had a popcorn party!  

Congratulations to Mrs. Logan"s, Mrs. Manier's and Mrs. Miller's classes were the first 3 classes to have 100% registration for the Mustang Trot!  

If you would like to volunteer please sign up here:  https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-mustang#/

#### Yearbooks for Sale!

Buy your 23/24 Marshdale Yearbook today! Follow this link to set up your account and receive 10% off any purchase before September 30.

https://www.treering.com/purchase?PassCode=1016528018343197

#### Save the Date

The Pumpkin Festival on October 15th, from 11-2 at Marshdale!

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
