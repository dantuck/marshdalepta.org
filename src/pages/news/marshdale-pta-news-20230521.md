---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - May 21, 2023
description: Marshdale PTA News - May 21, 2023
date: 2023-05-22
image: /img/news-default.webp
imageAlt: Marshdale PTA News - May 21, 2023
---

Happy Mother's Day to all of the caregivers from the Marshdale PTA.

#### School Supplies!

May 31st Deadline<br />
Takes 5 minutes

$5 from every box goes to the school to use towards safety enhancements
Hassle free! Your child's box will be waiting at their classroom  
The art supplies are already in each supply list so you will be all set!

Think you'd get a better deal shopping at Target, Walmart, or even on Amazon you won't....1st Day School Supplies secured their inventory in the fall, saving our school up to 30% off over big box stores, AND all name brand names you trust!

Order your 1st Day School Supplies kit  to save yourself time and money this year!

https://1stdayschoolsupplies.com/kits.php?sid=19008

#### Spirit Wear

Take advantage of the Marshdale Year-End Spirit Wear Sale! Gear up for the summer with shorts, tanks, water bottles and beach towels! Everything is 25% off! Shop Now: https://1stplacespiritwear.com/collection/16465/



----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
