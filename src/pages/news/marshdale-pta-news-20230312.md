---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Mar 12, 2023
description: Marshdale PTA News - Mar 12, 2023
date: 2023-03-13
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Mar 12, 2023
---

#### Yearbook

Order your yearbook today and you can work on the two custom pages that will only appear in your book. The Yearbook Team is excited to share their hard work with you. Orders and custom pages need to be completed by April 18. You can still order after April 18th, but you will have to pay for shipping directly to your home.

Follow this link to order: 
https://www.treering.com/purchase?PassCode=1016528018343197

#### School Store

The RISE School Store needs parent volunteers to help on Wednesday and Thursday, March 15th and 16th.  The kids have been earning RISE dollars and are excited to spend them.

Please review the available slots:
https://www.signupgenius.com/go/5080a45adaa28a2f49-marshdale1/139797416#/

#### Spring Auction

Auction tickets on sale now and limited space available for child care during the auction.

https://app.memberhub.gives/spring-auction

Money raised will pay for our new basketball court!

Would you like to donate a product or service for the Spring Auction?  Contact lara.nusbaum@marshdalepta.org

Volunteer Spring Auction: https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-spring

<u>**Here are a few of our Live Auction Items:**</u>

<u>**Frisco Getaway**</u>: Three nights stay in a top floor condo in the Frisco Mountainside complex (July 28, 29, and 30th) Two Bedrooms (main bedroom with king-sized bed and loft with queen bed), one Bathroom, and fully equipped kitchen.  Reserved covered parking spot. Clubhouse has an indoor pool, 3 outdoor hot tubs, sauna, steam room, racquetball court, and tennis court.  

Mountainside is a short walk to Frisco's Main Street with shopping and dining options.  Frisco is centrally located in Summit County with easy access to multiple ski resorts that provide a variety of summer activities including; aline roller coasters, lift assisted downhill mountain biking, hiking trails, climbing walls, boating and more!  The condo is a 3 minute walk/ride to the Tenmile Bikepath to Copper Mountain and the Blueriver Pathway to Breckenridgeand and many other scenic bike paths and hiking trails.  15-minute walk to Frisco marina and the Frisco Adventure Park.

You will also get a $25 gas card from Circle K, a Jordie bike tune up from Outbound Mountain Gear, travel games from Critter's Toy Store and a bottle of wine from Creekside Cellars!


<u>**Private Yoga**</u> session with a wine and cheese party afterwards!  Enjoy an 1 hour yoga session at your home with 5 friends with Third Flight Yoga at Mac Nation.  You get 2 Afterwards enjoy 2 bottles of wine from Scout & Cellar and cheese with your friends.


<u>**Get Moving**</u>:  A beautiful gift basket from Royal Savage including a Hat, Crop Hoodie, Sports Bra, Leggings, Water Bottle and Belt bag.  Royal Savage is local to Evergreen, Female owned, and completely one of a kind. Each design and pattern has been uniquely designed for maximum comfort and usability. The goal for Royal Savage, and for life, is to encourage and empower you to be yourself, be powerful and above all else, go savage!  You will also get a month pass from Anytime Fitness in Conifer, a foam roller from Select Physical Therapy in Conifer and a healthy choices gift basket from Natural Grocers.

<u>**Police Chauffeur**</u>:  What kid wouldn't want to get driven to school in a police car?  The Jeffco Sheriff's office will have your child(ren) driven to school in style when they can to explore the police car and then ride from their house to school in the police car! 

<u>**Bowling and Pizza with Mr. Martin**</u>:  Your child will get some extra quality time with our wonderful Principal, Mr. Martin.  They will meet at Wild Game and have fun bowling and then enjoy pizza afterwards.

<u>**Domino's Pizza lunch on the roof with Mr. Martin**</u>.  Your child and a friend will have Domino's Pizza on the roof of Marshdale Elementary.  

<u>**1/2 day of Disc Golf with Mr. Martin**</u>.  Nothing better than enjoying an afternoon playing disc golf with Mr. Martin including a picnic lunch and a snack.

<u>**Principal of the Day**</u>:  Yes, your child can become our school's leader for the day!

<u>**Ride-a-long in a police car**</u>:  You will get a chance to ride in a Jeffco Sheriff's car and see what their day takes them.  Must be 16 or older.

<u>**Pony Birthday Party**</u>:  Your child will have 2 very special guests at their party:  Derringer and Sam the ponies!  Kids can ride them, learn about ponies and play with them.  Your party will also include Domino's Pizza and a dozen cupcakes from Mountainside Bakery.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
