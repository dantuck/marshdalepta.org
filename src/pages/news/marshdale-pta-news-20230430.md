---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Apr 30, 2023
description: Marshdale PTA News - Apr 30, 2023
date: 2023-05-01
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Apr 30, 2023
---

#### Teacher Appreciation

Teacher and Staff Appreciation is going to be celebrated at Marshdale on May 1-5th. PTA has several activities planned to say "Thank You" to our wonderful teachers and staff. We are asking you to please celebrate your teachers and our staff on Thursday, May 4th with homemade cards, notes or pictures. 

Also, Principals' Day is May 1st, if your family would like to recognize Mr. Martin. 

#### School Supplies for Next Year

Order next year's school supplies with one click of a button and they will be at your child's classroom on the first day back! $5 of every box goes to the school to purchase safety enhancements for the school.

https://1stdayschoolsupplies.com/kits.php?sid=19008

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
