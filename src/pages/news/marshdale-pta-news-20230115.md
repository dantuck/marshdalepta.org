---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Jan 15, 2023
description: Marshdale PTA News - Jan 15, 2023
date: 2023-01-16
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Jan 15, 2023
---

#### Join the PTA!

Marshdale PTA Spring Membership Drive is underway! Join today! The class with the most new members wins a pajama party. This is for new members only. If you already joined in the fall, you are already awesome!

https://www.marshdalepta.org/membership/

#### Marshdale Skate the Lake

Join your friends and classmates at Evergreen Lake on February 17th from 1pm - 3pm. Snacks and refreshments will be provided. The cost to skate is $15/person which includes skate rental. Please be advised there are a limited number of skates for the younger kids. 

Please RSVP [here](https://docs.google.com/forms/d/e/1FAIpQLSexzr-dX4JfBRjiIydrdED7rTBddhvF3Mo0dAygBSLPJ3UWNw/viewform?usp=sf_link).

#### Science Fair

Mark your calendars for the **2023 Marshdale Science Fair**! It's never too early to begin working on your project. Register your project plan by February 8th, and final projects will be due February 22nd. **Pick up your free display board from the office**.

Find complete details at https://www.marshdalepta.org/enrichment/science-fair/

Even if your child is entering a project, please consider volunteering to help make our Science Fair a success. If you are willing to give 30 minutes to 2 hours of your time to help judge the projects, please sign-up for one of our judging sessions. https://www.signupgenius.com/go/4090E45A4A72DA7FC1-marshdale

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
