---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Sept 24, 2022
description: Marshdale News - Sept 24, 2022
date: 2022-09-24
image: /img/2022-mustangtrot-logo.webp
imageAlt: Marshdale Mustang Trot - Get moving fundraiser for the whole school!
---

**Mustang Trot**:  We have all but a couple students in the school registered!  If you have not registered your child yet please do so today.  Classrooms with 100% registration will receive an extra recess.  No obligation to fundraise, but please register so your child's class gets an extra recess! It literally takes 3 minutes especially if you have registered in the past...you just need to sign up for the 2022 year.

https://www.getmovinfundhub.com/register?school_uuid=5d4322860edfc

And for those of you that want to fundraise, remember sharing your link on social media is the easiest and fastest way to do so!  And, every time you share the link, your child receives an entry in the "Firefighter for the Day" drawing.  We made our next goal so Mr. Martin served lunch in a special red chef's hat!  Kids loved it!  Congratulations to Mrs. Baynes' Class...they raised the most last week so it is their turn to have the Mustang Trot traveling trophy and will have a popcorn treat tomorrow!  We are over 70% of our goal!  Remember the money we raise goes towards supporting our wonderful staff and students by purchasing the many enriching elements that are needed day to day:  continued special programs, updating technology, licensing for educational programs!

**Mustang Trot sign up to volunteer**:

https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-2022

**Legacy parents**:  If you went to Marshdale and your child now goes to Marshdale please email lara.nusbaum@marshdalepta.org.  We are looking for legacy parents to tell their story as part of our celebration of the old school.

**Mustang Monday** was a huge hit so we are doing it again!  Mark your calendars for Monday, October 3rd from 5-7pm at Marshdale Burger.  Come get to know other parents and let the kids play while you just enjoy everyone's company, a drink or maybe grab something to eat.  Everyone is responsible for their own purchases.

**New Marshdale Elementary Logo contest**.  We invite anyone that would like to participate to submit a new logo for Marshdale Elementary.  We are keeping with our mustang mascot.  The submission must be a completely original piece of artwork.  May not be traced or copied from someone else's work.  If you would like to submit your entry please do so before October 13th to the office.  Anyone in the community is welcome to make a submission.  Students, Staff and PTA Members will vote on the best one October 26-27 and the winner will be announced the week of October 31st.

Looking for something to do with the kids?  Please go to **Mountain Blizzard Pinball** in Conifer.  Because of their generous donations of the 1/2 hour of free play as a Trot Prize we were able to shift the money we saved to do some really fun things on the last day in the old school and first day in the new school.  Please go in and thank owners Laura and Kevin for their support of our school!   If you want a fun place to host a birthday party or special event you can book their party room.  They have 39 pinball machines and other arcade games for you to enjoy!

#### JOIN THE PTA!

Become a member of the PTA. Membership affords you voting rights and access to information, resources and programs. No volunteering required!

https://marshdalepta.memberhub.com/store?limit=21&live=true

**Congratulations, Mrs. Ericson's Class!** They won the PTA membership drive classroom contest and will enjoy a pajama day. Nice work, Mrs. Ericson's class!

**PTA General Meeting** - October 11 at 5:30 pm in the LMC. We will be presenting and voting on our annual budget. Being a PTA member affords you voting rights so join today!

#### Reflections

Do your students like to express themselves through the arts? The PTA Reflections Art Program allows students in grades P-12 to enter pieces in visual arts, photography, literature, film production, dance choreography, and music composition. Winners at the school level advance to district, then state and national, with a chance to win awards and scholarships. The 2022-2023 theme is "Show Your Voice!" Projects are due November 9. Find complete details and links to helpful resources at https://www.marshdalepta.org/enrichment/reflections/

#### RISE School Store

On Friday, September 30, we will hold our first RISE store where students are able to spend their RISE dollars on fun incentives. If you are interested in volunteering to make this happen, please sign up on the following link: https://www.signupgenius.com/go/70A054AA4AE22A1FF2-rise

#### Every Kid Outdoors Pass

**[Every Kid Outdoors Pass](https://everykidoutdoors.gov/index.htm)** - Free entrance to national parks and national forests for 4th graders! 