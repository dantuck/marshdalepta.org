---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Oct 16, 2022
description: Marshdale News - Oct 16, 2022
date: 2022-10-17
image: /img/news-default.webp
imageAlt: Marshdale News - Oct 16, 2022
---

Congratulations, Ericson's Eagles! Not only did the Ericson Eagles raise more money than any class, they more than doubled their goal! Mrs. Ericson's class raised $4,350 which is 207.14% of your goal! PTA will cover Mrs. Ericson's afternoon recess for a week. 

Please help donate food Oct. 24th for retired teachers and alumni that will be at the ribbon cutting ceremony and help with extra recess on November 1st.

<a href="https://www.signupgenius.com/go/10c0e44a8a62aa1f4cf8-breakfast" target="_blank">https://www.signupgenius.com/go/10c0e44a8a62aa1f4cf8-breakfast</a>

#### Pumpkin Decorating Contest

Pumpkins are due October 25th, winners receive a gift bag from Critters.

<a href="https://www.marshdalepta.org/assets/2022_Pumpkin_Decorating_Flyer.pdf" target="_blank">https://www.marshdalepta.org/assets/2022_Pumpkin_Decorating_Flyer.pdf</a>

#### Marshdale Pumpkin Festival

Join the Marshdale community for the annual Pumpkin Festival on October 30th from 11am - 2pm. <a href="https://marshdalepta.memberhub.com/store/items/784277" target="_blank">Details</a> and <a href="https://www.signupgenius.com/go/4090a49aeae2ba3ff2-pumpkin" target="_blank">Volunteer Opportunities</a>

**Join the Marshdale PTA** - https://www.marshdalepta.org/membership/

Show your voice! Students have the chance to express themselves through art in the Reflections program. Projects due November 9. Find complete details at https://www.marshdalepta.org/enrichment/reflections.

#### Marshdale's yearbook is now available to purchase! 

Order by October 31 to receive 10% off. Design two custom pages for free that will show up only in your book. Go to <a href="https://www.treering.com/validate" target="_blank">https://www.treering.com/validate</a> and enter our passcode: **1016528018343197**

Calling all parents! Are you interested in bringing a Destination Imagine team to Marshdale? See the attached flyer for details about an informational meeting. Please reach out to <a href="mailto:education@marshdalepta.org">education@marshdalepta.org</a> so we can brainstorm developing a team at our school https://www.marshdalepta.org/enrichment/activities/
