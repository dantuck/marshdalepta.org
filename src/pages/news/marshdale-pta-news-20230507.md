---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - May 07, 2023
description: Marshdale PTA News - May 07, 2023
date: 2023-05-08
image: /img/news-default.webp
imageAlt: Marshdale PTA News - May 07, 2023
---

#### General Meeting

Please join us for our final general meeting of this school year on Tuesday, May 9th at 5:30 pm at Marshdale Elementary School. 

#### Teacher Appreciation

Thank you to everyone who helped our teachers, staff & principal feel appreciated this past week. Our teachers and staff are amazing, thank you for helping us remind them of that. 

#### School Store

Thank you to all of our RISE School Store volunteers this past semester. Your time is greatly appreciated. 

#### School Supplies

Order next year's school supplies with one click of a button and they will be at your child's classroom on the first day back! $5 of every box goes to the school to purchase safety enhancements for the school.

https://1stdayschoolsupplies.com/kits.php?sid=19008&utm_source=19008&utm_medium=email&utm_campaign=repfwd3

#### Reusable Bags

Marshdale reusable bags are here! In Friday folders there was a flyer sent out explaining how to order your bag(s).  With each $5 donation you will receive one bag. Thank you to Michele Vanags of State Farm in Evergreen for paying for the bags so all money donated goes to our new special project: Growing Dome!  Didn't get a flyer? No problem, just send a check written out to Marshdale PTA (in the memo write Marshdale Bag) and a note of your student's name and teacher and next week they will be sent home.

#### Marshdale PTA Board 2023-24

We are looking for an interested parent/caregiver of a Marshdale student to join the PTA board next year. Open position: VP of Fundraising 

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
