---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Feb 19, 2023
description: Marshdale PTA News - Feb 19, 2023
date: 2023-02-20
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Feb 19, 2023
---

#### PTA General Meeting

Please join the PTA for the next general board meeting on Tuesday, February 21st at 5:30 at school.  Babysitting will be available and we will be hearing from Amy Temple, our Social Emotional Learning Specialist and School Psychologist.

Need childcare so you can attend the PTA general meeting? We've got you covered! Fill out this [FORM](https://docs.google.com/forms/d/e/1FAIpQLSf9c-dzUGUcot9BilfMkyD2CgGivHlD8U4DDNuh_K03-raMjw/viewform?vc=0&c=0&w=1&flr=0) to let us know your childcare needs.

#### Science Fair

The Marshdale Science Fair will be held on February 22nd and 23rd. If your student is registered to participate and you did not receive an email with details, please email wendi.vanlake@marshdalepta.org.

#### A Day Without Hate

A Day Without Hate is a Jeffco student-led, grassroots organization that promotes nonviolence, respect, and unity within our schools. Marshdale's student leadership will lead our school in activities surrounding these values on April 27th. If you would like to order a Day Without Hate t-shirt (proceeds go to scholarships for Jeffco students), forms will be found in your child's Friday Folder this week. Orders are due by March 8th.

#### Yearbook

Order your yearbook today and you can work on the two custom pages that will only appear in your book. The Yearbook Team is excited to share their hard work with you. 

Follow this link to order: https://www.treering.com/purchase?PassCode=1016528018343197

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
