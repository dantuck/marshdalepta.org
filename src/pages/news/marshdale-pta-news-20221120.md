---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Nov 20, 2022
description: Marshdale PTA News - Nov 20, 2022
date: 2022-11-21
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Nov 20, 2022
---

#### Reflections

Thank you to all the students who participated in this year's PTA Reflections program. These students were able to express themselves through the arts with the theme: "Show Your Voice." The top three contestants in each category and division advance to the Jeffco PTA competition, with the chance to continue to state and national levels. This year's advancing students are Emmot Tucker (Kindergarten), Chase Somogyi (2nd Grade), Hayden Davidson (3rd Grade), Amity Newes (5th Grade), Faye Van Airsdale (5th Grade), Harper Somogyi (4th Grade), Maya Baer (3rd Grade), and Macy Vakoc (3rd Grade). You can see these students' artwork on display at Red Rocks Community College in Lakewood from December 12-16. Congratulations Marshdale artists!

#### Original Works Artwork

Order by November 21st. Holiday gifts with your child's original artwork on it! All items ordered will be sent home before Winter Break. Half of the money we raise for this will go directly to our art program. Last year, it was used for Ms. Chew to go to an enrichment seminar. The other half goes to the Rise program.

Order forms went home in Friday folders last week.

#### Spirit Wear - Holiday Gifts for The Whole Family!

Save 20% on every single item in the store and get free shipping on orders over $75 during the Marshdale Elementary School holiday sale!

Our new Family Designs line features 10 new graphics that can be customized for Mom, Dad, Parent, Grandma, Grandpa, Grandparent, Sister, Brother, Aunt, Uncle, Teacher, Coach and Alumni!

All orders placed in November are GUARANTEED delivery before the holidays or it's FREE. Current production times are less than 1 week! [Shop Now](https://1stplacespiritwear.com/schools/CO/Evergreen/Marshdale+Elementary+School/collection_detail.html?type=Family%20Designs&utm_source=customer-db&utm_medium=email&utm_campaign=fwd2).

 

#### Spring Auction 2023

Spring Auction Committee Zoom Planning Meeting:  Thursday, December 8th, 6:30-7:30pm. No matter how much time you have to volunteer we can use you!  There are several things that the committee needs to do on people's own timeline.  The Spring Auction is our second biggest fundraiser of the year and one of the most fun!   If you would like to be involved, contact Lara.nusbaum@marshdalepta.org. 

----

Like and follow the Marshdale PTA on Facebook 

https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page 

https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
