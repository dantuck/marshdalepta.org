---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Aug 27, 2023
description: Marshdale PTA News - Aug 27, 2023
date: 2023-08-28
---

#### Membership

The Marshdale PTA Membership Drive is now OPEN! [Join the PTA](/membership/) - all the rights with none of the requirements.

The class with the most PTA members wins a Popcorn Party - Deadline is September 29th!

Family Membership $35 (includes 2 members)<br/>
Individual Caregivers $20<br/>

https://marshdalepta.memberhub.com/store?limit=21&live=true

#### Mustang Trot

We hit our first 2 goals!  And Thursday afternoon gave away several registration prizes!  Not registered yet?  Easy to do and you are entered in next week's drawing.  We are raising money for safety enhancements at the school. http://app.memberhub.gives/mustang-trot

Trot T-Shirt contest entries due by September 1st. [download](/assets/2023-mustangtrot-t-shirt-design-contest-form.pdf)

#### Spiritwear

Marshdale Spiritwear 30% off until September 7th and free shipping with minimum order! https://1stplace.sale/16465

#### Big Chili & Marshdale PTA

Marshdale Elementary PTA is going to be selling water at the Big Chili Cook-Off on September 9th. Help volunteer and get in for free! https://www.signupgenius.com/go/10c0e44a8a62aa1f4cf8-water#/

#### Save the Date - Patriot's Day

Marshale will host our annual Patriot's Day ceremony and breakfast for our local heroes on Monday, September 11th.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
