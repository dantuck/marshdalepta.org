---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Oct 02, 2022
description: Marshdale News - Oct 02, 2022
date: 2022-10-03
image: /img/news-default.webp
imageAlt: Marshdale News - Oct 02, 2022
---

**Help Us Break a Record - [Join the PTA!](https://www.marshdalepta.org/membership/)**


#### Mustang Trot

October 7th. 9:10am-10:10am:  Wear your grade color with your Trot T-Shirt! 

This is our last week to fundraise. We are at 80% our goal and 97% students are registered.  There is no obligation to fundraise, but please register your child so their whole class gets an extra recess.  If you can share your link on social media to fundraise, please do so this weekend so we can make our goal. Congratulations to Mrs. Moore's Class for raising the most money this week.

https://www.getmovinfundhub.com/register?school_uuid=5d4322860edf

#### Mustang Trot Fundraising

We are raising money for school enrichments that help support our teachers and students every day. Examples:  updating technology, licensing for educational programs and $350 to each teacher to use for items they need in their classrooms…special projects or seminars they would like to attend. 

**Next week is Mustang Trot.  October 7th 9:10 - 10:00 am at the Marshdale Park Field.**  

#### Volunteer

Time or donate items needed https://www.signupgenius.com/go/10c0e44a8a62aa1f4cf8-2022 

#### Wear Your Grade Color for the Trot

We are celebrating the colors of the rainbow this year!  Each grade will represent a different color of the rainbow.  Trot T-Shirts are red this year and will go home on Thursday, October 6th.  Please make sure they wear it on Friday, October 7th.  They should also wear as much of their grade color as they wish (shoes, colored hair, face paint, pants, bandana, hat…be as creative as you like).

5th Grade:  Red

4th Grade:  Orange

3rd Grade:  Yellow

2nd Grade:  Green

1st Grade:  Blue

Kindergarten:  Violet 

#### Trot Signs

We will not be hanging signs this year.  We encourage parents that can attend the Mustang Trot to make signs for their students and hold them as they cheer them on.  Please make sure you take the signs with you when you leave the field.

#### Spirit Days

October 13 and October 24th!  Wear red and black to show your school spirit!

https://1stplacespiritwear.com/schools/CO/Evergreen/Marshdale+Elementary+School

#### Marshdale Monday

Oct. 3rd 5 - 7 pm at Marshdale Burger.  Come meet new friends or talk with old ones.  Come for just the conversation or stay for dinner.  Everyone is responsible for their own purchases.

#### Marshdale Student Alumni at WJ Middle School or Conifer HS

Make a paper memory and visit the old school.  See your weekly school newsletter for more information.

#### Show your voice! Reflections 2022

Students have the chance to express themselves through art in the Reflections program. Projects due November 9. Find complete details at
https://www.marshdalepta.org/enrichment/reflections.