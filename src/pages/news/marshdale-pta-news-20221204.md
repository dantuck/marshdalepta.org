---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Dec 04, 2022
description: Marshdale PTA News - Dec 04, 2022
date: 2022-12-05
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Dec 04, 2022
---

#### Enrichment Activities

Several programs provide after-school enrichment opportunities for our Marshdale students. New programs and new sessions of existing programs are beginning in January.

Check out the offerings at https://www.marshdalepta.org/enrichment/activities/

#### Science Fair

Mark your calendars for the 2023 Marshdale Science Fair! It's never too early to begin working on your project. Register your project plan by February 8, and final projects will be due February 22.

Find complete details at https://www.marshalepta.org

#### Yearbook
Buy your yearbook today! Each yearbook will have two free customizable pages that will only be printed in your book. If you choose to donate a book to a Marshdale student in need by December 31, Treering will match your donation. Just click on "donate a yearbook" at checkout. 

https://www.treering.com/purchase?PassCode=1016528018343197

### Spring Auction

Virtual Spring Auction committee meeting Thursday, December 8th 6:30-7:30pm.  Be a part of the planning.  Email Lara.nusbaum@marshdalepta.org if you would like the zoom link.

----

Like and follow the Marshdale PTA on Facebook 

https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page 

https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
