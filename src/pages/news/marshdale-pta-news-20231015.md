---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Oct 15, 2023
description: Marshdale PTA News - Oct 15, 2023
date: 2023-10-16
---

#### Pumpkin Festival

Thank you to everyone who attended the Pumpkin Festival today. Thank you to all of our volunteers that helped pull off a fun community event!

#### Parent Teacher Conference Teacher Meals

Please consider helping the Marshdale PTA feed our hard working teachers and staff on parent teacher conferences nights. https://www.signupgenius.com/go/5080A45ADAA28A2F49-45040315-parentteacher

PTA Shout Outs

Thank you, Ashley Remington for all of your hard work on the Pumpkin Festival!<br />
Thank you, Christina Haydon, for taking such good care of our students!<br />
Thank you, Amy Temple, for coordinating Dr. Cyper's presentation on childhood anxiety & depression.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
