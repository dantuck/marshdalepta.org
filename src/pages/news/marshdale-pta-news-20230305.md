---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Mar 5, 2023
description: Marshdale PTA News - Mar 5, 2023
date: 2023-03-06
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Mar 5, 2023
---

#### Parent Teacher Conference Meals for Teachers & Staff

Parent/Teacher Conferences are coming up on March 2nd and March 9th.  We will be providing dinner for our hard working teachers. 

If you would like to contribute to the dinners please sign up at https://www.signupgenius.com/go/5080a45adaa28a2f49-parentteacher1#/

#### Spring Auction

Early bird tickets on sale now through March 10th.

https://app.memberhub.gives/spring-auction. 

Money raised will pay for our new basketball court!

Would you like to donate a product or service for the Spring Auction?  Contact lara.nusbaum@marshdalepta.org

Volunteer Spring Auction: https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-spring

#### A Day Without Hate T-Shirts

If you would like to order a Day Without Hate t-shirt (proceeds go to scholarships for Jeffco students), forms were in your child's Friday Folder or can be found here: https://drive.google.com/file/d/1FyhxVBm4MYjonLG2-DvV5T7pSlSTuV3v/view?usp=share_link. Orders due no later than this Wednesday, March 8th. 

Contact wendi.vanlake@marshdalepta.org if you have any questions.

#### After School Enrichment

Want to learn to play the guitar? Interested in exploring nature? New sessions of Eric Stone Guitar and Evergreen Audubon Explore More are beginning on Wednesdays this month. Visit https://www.marshdalepta.org/enrichment/activities/ for complete details and registration.

#### Marshdale Green Team

The Marshdale Green Team meeting is on Friday March 10th from 3:40 - 4:40 in the art room (students and parents welcome). This month's topic is on sustainable design features of the new school! We'll also be talking about Earth Day activities for the school in April. Contact Rachel Franchina at rfranchina@gmail.com or Laura Clark at laclark818@gmail.com to be added to the Green Team list. 

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
