---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - May 14, 2023
description: Marshdale PTA News - May 14, 2023
date: 2023-05-15
image: /img/news-default.webp
imageAlt: Marshdale PTA News - May 14, 2023
---

Happy Mother's Day to all of the caregivers from the Marshdale PTA.

#### School Supplies

Order next year's school supplies with one click of a button and they will be at your child's classroom on the first day back! $5 of every box goes to the school to purchase safety enhancements for the school.

https://1stdayschoolsupplies.com/kits.php?sid=19008

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
