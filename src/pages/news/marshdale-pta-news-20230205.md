---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Feb 05, 2023
description: Marshdale PTA News - Feb 05, 2023
date: 2023-02-06
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Feb 05, 2023
---

Congratulations to Ms. Moore's Kindergarten class for winning the PTA Membership Drive Pajama Party!

#### PTA General Meeting

Please join the PTA for the next general board meeting on Tuesday, February 21 at 5:30 at school.  Babysitting will be available and we will be hearing from Amy Temple, our Social Emotional Learning Specialist and School Psychologist.

#### Marshdale Monday @ Snowpack Taproom

Please join us for Bingo at Snowpack Taproom on Monday, February 13th from 5-8 pm. Snowpack will donate 10% of their sales to Marshdale. 

#### Marshdale Skate the Lake

Join your friends and classmates at Evergreen Lake on February 17th from 1pm - 3pm. Snacks and refreshments will be provided. The cost to skate is $15/person which includes skate rental. Please be advised there are a limited number of skates for the younger kids. 

Please RSVP [here](https://docs.google.com/forms/d/e/1FAIpQLSexzr-dX4JfBRjiIydrdED7rTBddhvF3Mo0dAygBSLPJ3UWNw/viewform?usp=sf_link).

#### Science Fair Registration

If your student is participating in this year's Science Fair, make sure you register the project by this Wednesday, February 8th. Either turn in the paper registration form to the office or register electronically here: [2023 Science Fair Registration](https://docs.google.com/forms/d/e/1FAIpQLSdYZnVBhSDjn6lWbsrHhvzVgFrS8WpiMSim9-D-O1hbCaR1gw/viewform).

Don't forget to pick up your free display board from the office! Final projects are due at 9 am on Wednesday, February 22nd.

Also, to make this fair successful, we need volunteer judges. If you are able to help judge the projects on February 22nd or 23rd, please sign up here: https://www.signupgenius.com/go/4090E45A4A72DA7FC1-marshdale.

#### Enrichment 

Marshdale partners offer a variety of after-school activities at our school. New sessions of Kinetic Dance, Audubon Explore More, and Guitar are starting soon. We also have a new Juggling Club starting February 21nd! Visit https://www.marshdalepta.org/enrichment/activities/ for complete details and registration links.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
