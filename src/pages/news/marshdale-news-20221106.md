---
layout: '../../layouts/BlogPost.astro'
title: Marshdale News - Nov 6, 2022
description: Marshdale News - Nov 6, 2022
date: 2022-11-07
image: /img/news-default.webp
imageAlt: Marshdale News - Nov 6, 2022
---

Thank you to Laura Squires & Silver Spoon Catering for feeding our hardworking teachers. Thank you to all parents who contributed food for Parents Teacher Conferences on Thursday night!

#### 2022 Reflections Theme: Show Your Voice! 

Reflections projects are due this Wednesday, November 9. Turn in your completed project Wednesday morning in the front office. Make sure your name is not on the front of your piece, and don’t forget to include a filled-out entry form. Extra copies of the form are in the office. All forms, rules, and a check-list can be found at: https://www.marshdalepta.org/enrichment/reflections.

#### Veteran's Day

In honor of Veterans Day on November 11th, Marshdale Elementary will display a “Wall of Heroes.” Each family is invited to write a biography (or biographies) to honor family members, or friends, who have served, or are currently serving in the United States Armed Forces.

Please return the biographies by Thursday, November 10th, so we can display them for Veterans Day. The form was sent home in Friday Folders. Extra forms are in the office, or you can email the front desk and they will email you a copy. Thank you for recognizing and honoring our veterans.

#### Green Team Meeting on November 11th

All students and their parents are invited to the next Green Team Meeting from 3:40-4:30 in the SSN room. This month's topic is Recycling and we will be making bird feeders from recycled materials and talking about recycling at our new school. If your child is attending without a parent, please email Rachel at rfranchina@gmail.com so we can notify the office.

A reminder that you can sign up to help up with composting on the 2nd Wednesday of each month. We still need another volunteer for this Wednesday, November 9th. The kids love having our friend at Purple Bucket help them separate food and we get the compost back in the spring! 
