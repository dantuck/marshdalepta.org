---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Mar 19, 2023
description: Marshdale PTA News - Mar 19, 2023
date: 2023-03-20
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Mar 19, 2023
---

#### Spring Auction

Want a new basketball court with adjustable baskets from 10 feet?  Help us raise money!

Auction tickets on sale now and limited space available for child care during the auction.

https://app.memberhub.gives/spring-auction

Money raised will pay for our new basketball court!

Would you like to donate a product or service for the Spring Auction? Contact lara.nusbaum@marshdalepta.org

Volunteer Spring Auction: https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-spring

<u>**Here are a few of our Live Auction Items:**</u>

<u>**Get Moving and become a Royal Savage!**</u>: A full outfit from Royal Savage, gift basket of healthy choices from Natural Grocers, month membership to Anytime Fitness and a large foam roller from Select Physical Therapy.

<u>**2 nights under the stars!**</u>: 2 nights at campsite #20 with gorgeous views at Staunton State Park, 2 solar chargeable lanterns with usb port, portable corn hole game, camping scavenger hunt game, collapsible sink and 3 kids walkie talkies!

To see the full list so far go to the ticket site and you can see all the live and silent auction items listed.

#### Mountain Area Science Fair

Congratulations to all the Marshdale Scientists who represented our school at the Mountain Area Science and Engineering Fair last weekend! The following students' projects placed in the top three of their division and category. Way to go, Marshdale!

<u>**3rd place**</u>

Mary Rolfing, Earth Science<br />
Hailey Davidson, Electricity and Magnetism<br />
Zane Brown, Aerodynamics<br />
Clare Janasiewicz, Chemistry<br />
Amelie Anderson, Material Science<br />
Cade Earley, Zoology

<u>**2nd place**</u>

Addie Hollabaugh, Physics

<u>**1st place**</u>

Cassidy Croxton, Chemistry

#### Yearbook Cover Design Contest

We are looking for art for the cover of the 2023 Marshdale yearbook. Our theme is "Life in the Mountains". If you would like to submit a cover for consideration, please use a piece of unlined paper in portrait orientation. Include your full name and grade on the back of your artwork. The winner will be determined by the yearbook staff. Submissions are due no later than Wednesday, March 29 to the PTA box in the office.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
