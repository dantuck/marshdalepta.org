---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Jan 08, 2023
description: Marshdale PTA News - Jan 08, 2023
date: 2023-01-09
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Jan 08, 2023
---

Welcome back, Marshdale! Happy New Year from the Marshdale PTA!

#### PTA Membership Drive!

Join the PTA! The class with the most new members will win a Pajama Day. If you joined back in the fall, no need to join again - this is for new members.
Flyers came home in Friday Folders.

https://www.marshdalepta.org/membership/

#### Reflections

Congratulations to Kindergartener Emmot Tucker for winning the Jeffco Reflections contest! He was recognized at a district level ceremony on Friday, December 16th and his visual arts project will advance to the state level. Way to go, Emmot!

#### Science Fair

Mark your calendars for the **2023 Marshdale Science Fair**! It's never too early to begin working on your project. Register your project plan by February 8th, and final projects will be due February 22nd. **Pick up your free display board from the office**.

Find complete details at https://www.marshdalepta.org/enrichment/science-fair/

#### Virtual Spring Auction Meeting

Thursday, January 19th from 6:30 - 7:30 pm Virtual meeting. Email lara.nusbaum@marshdalepta.org if you would like the link.

#### PTA Sponsorships & Donations

If you would like to be a sponsor and/or donate an item for the Spring Auction please email lara.nusbaum@marshdalepta.org.

#### Composting and Great Team Meeting

We're kicking off 2023 composting with the monthly lunch composting on Wednesday, January 11th. Two volunteers are needed to help kids sort items into compost and trash bins.

The Green Team Meeting is January 13th from 3:40 - 4:40 pm. This month's meeting will feature the Evergreen Sustainability Alliance puppet show and a discussion on how we want to share the puppet show with other students at Marshdale. Email Rachel Franchina at rachel.franchina@gmail.com or Laura Clark at laclark818@gmail.com to be added to the Green Team contact list.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
