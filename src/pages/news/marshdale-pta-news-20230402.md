---
layout: '../../layouts/BlogPost.astro'
title: Marshdale PTA News - Apr 02, 2023
description: Marshdale PTA News - Apr 02, 2023
date: 2023-04-03
image: /img/news-default.webp
imageAlt: Marshdale PTA News - Apr 02, 2023
---

#### Spring Auction

Tickets to the Spring Auction April 14th. We might just meet capacity this year! So don't delay and buy your tickets today!  Tickets are on sale until April 7th or when we sell out (we are not able to sell tickets the week before the auction or at the door).  Food, dancing, drinks and lots of cool live and silent auction items (see a list of items on ticket website). 

We are raising money for our new basketball court! https://app.memberhub.gives/spring-auction. 

Would you like to volunteer at the Spring Auction or help before the auction? We need you!

https://www.signupgenius.com/go/10C0E44A8A62AA1F4CF8-spring

To see the full list so far go to the ticket site and you can see all the live and silent auction items listed.

#### Yearbook

Order your yearbook today and you can work on the two custom pages that will only appear in your book. The Yearbook Team is excited to share their hard work with you. Orders and custom pages need to be completed by April 18. You can still order after April 18th, but you will have to pay for shipping directly to your home.

Follow this link to order: 
https://www.treering.com/purchase?PassCode=1016528018343197

#### 5th Grade Continuation/Graduation

5th grade parents - In your student's Friday folder was a form to get a free personalized graduation sign from the Marshdale PTA.  They must be filled out and turned back in by April 12th to receive your free sign.

#### 5th Grade Parents 

West Jeff Middle School PTA has several open positions: President, Treasurer, VP of Communications, VP of Enrichment & VP of Fundraising. Reach out to WJMSPTA@gmail.com if interested.

----

Like and follow the Marshdale PTA on Facebook <br />
https://www.facebook.com/marshdaleelementarypta

There is also a Marshdale Elementary Community Facebook page <br /> 
https://www.facebook.com/groups/593868314340780

Please respond to the admin questions.
