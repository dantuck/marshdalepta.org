import vue from "@astrojs/vue"
import sitemap from "@astrojs/sitemap"

export default {
  site: "https://marshdalepta.org",
  integrations: [
    vue(),
    sitemap(),
  ],
  vite: {
    ssr: {
      external: ["svgo"],
    },
  },
  trailingSlash: "always",
};
